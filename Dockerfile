FROM openjdk

WORKDIR /app

COPY out/artifacts/app_jar/app.jar /app/spring-app.jar



ENTRYPOINT ["java", "-jar", "spring-app.jar"]