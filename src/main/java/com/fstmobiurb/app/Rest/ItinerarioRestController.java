package com.fstmobiurb.app.Rest;

import com.fstmobiurb.app.model.Itinerario;
import com.fstmobiurb.app.model.LinhaDeOnibus;
import com.fstmobiurb.app.service.ItinerarioService;
import com.fstmobiurb.app.model.util.ItinerarioContentParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;

@RestController
@RequestMapping(value = "api/itinerario")
public class ItinerarioRestController {

    private  ItinerarioContentParser itinerarioContentParser = new ItinerarioContentParser();
    @Autowired
    private LinhaDeOnibusRestController linhaDeOnibusRestController;

    @Autowired
    private ItinerarioService itinerarioService;

    @RequestMapping(value = "/{id}")
    public Itinerario getItinerarioById(@PathVariable Integer id){
            Itinerario itinerario = new Itinerario();
            String response = itinerarioService.findItinerarioById(id);
            itinerario = itinerarioContentParser.itinerarioParser(itinerario, response);




        return  itinerario;
    }

    @RequestMapping(value = "")
    public ResponseEntity<Collection<Itinerario>> getAllItinerarios() {

        Collection<LinhaDeOnibus> linhasDeOnibus = linhaDeOnibusRestController.getAllOnibusLinhas();
        Collection<LinhaDeOnibus> linhasDelotacao = linhaDeOnibusRestController.getAllLotacaoLinhas();
        Collection<Itinerario> itinerarios = new ArrayList<Itinerario>();

        for (LinhaDeOnibus linha : linhasDeOnibus) {

            Itinerario itinerario = getItinerarioById(linha.getId());
            itinerarios.add(itinerario);

            try {
                Thread.sleep(60);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

        for (LinhaDeOnibus linha : linhasDelotacao) {

            Itinerario itinerario = getItinerarioById(linha.getId());
            itinerarios.add(itinerario);

            try {
                Thread.sleep(60);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }



        return new ResponseEntity<Collection<Itinerario>>(itinerarios, HttpStatus.OK);
    }

    @RequestMapping(value = "/onibus/")
    public ResponseEntity<Collection<Itinerario>> getAllOnibusItinerarios() {

        Collection<LinhaDeOnibus> linhasDeOnibus = linhaDeOnibusRestController.getAllOnibusLinhas();
        Collection<Itinerario> itinerarios = new ArrayList<Itinerario>();

        for (LinhaDeOnibus linha : linhasDeOnibus) {

            Itinerario itinerario = getItinerarioById(linha.getId());
            itinerarios.add(itinerario);

            try {
                Thread.sleep(60);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }


        return new ResponseEntity<Collection<Itinerario>>(itinerarios, HttpStatus.OK);
    }

    @RequestMapping(value = "/lotacao/")
    public ResponseEntity<Collection<Itinerario>> getAllLotacaoItinerarios() {

        Collection<LinhaDeOnibus> linhasDelotacao = linhaDeOnibusRestController.getAllLotacaoLinhas();
        Collection<Itinerario> itinerarios = new ArrayList<Itinerario>();



        for (LinhaDeOnibus linha : linhasDelotacao) {

            Itinerario itinerario = getItinerarioById(linha.getId());
            itinerarios.add(itinerario);

            try {
                Thread.sleep(60);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }



        return new ResponseEntity<Collection<Itinerario>>(itinerarios, HttpStatus.OK);
    }
}
