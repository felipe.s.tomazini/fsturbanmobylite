package com.fstmobiurb.app.Rest;

import com.fstmobiurb.app.model.Geolocal;
import com.fstmobiurb.app.model.Itinerario;
import com.fstmobiurb.app.model.LinhaDeOnibus;
import com.fstmobiurb.app.service.LinhaDeOnibusService;
import com.fstmobiurb.app.model.util.GeoLocalization;
import com.fstmobiurb.app.model.util.LinhaDeOnibusContentParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;


@RestController
@RequestMapping(value = "api/linhas")
public class LinhaDeOnibusRestController {
    @Autowired
    private LinhaDeOnibusService linhaDeOnibusService;
    @Autowired
    private ItinerarioRestController itinerarioRestController = new ItinerarioRestController();

    private LinhaDeOnibusContentParser linhaDeOnibusContentParser = new LinhaDeOnibusContentParser();

    @GetMapping(value = "/onibus/")
    public Collection<LinhaDeOnibus> getAllOnibusLinhas(){


        Collection <LinhaDeOnibus> linhasDeOnibus = new ArrayList<>();
        String sLinhasDeOnibus =  linhaDeOnibusService.findAllOnibus();
        String dividido[] = sLinhasDeOnibus.split("},");
        for (String pedaco: dividido) {
            LinhaDeOnibus linhaDeOnibus = new LinhaDeOnibus();
            linhasDeOnibus.add(linhaDeOnibusContentParser.linhaDeOnibusParser(linhaDeOnibus, pedaco));
        }

        return linhasDeOnibus;
    }

    @GetMapping(value = "/onibus/{name}")
    public ResponseEntity<Collection<LinhaDeOnibus>> getOnibusLinhasByName(@PathVariable String name){

      Collection<LinhaDeOnibus> linhasDeOnibus =  getAllOnibusLinhas();

        Collection<LinhaDeOnibus> result = new ArrayList<LinhaDeOnibus>();


        for (LinhaDeOnibus onibus:linhasDeOnibus) {

            if(onibus.getNome().contains(name) == true){
                result.add(onibus);

            }
        }


        if(result.isEmpty()){
            return new ResponseEntity<Collection<LinhaDeOnibus>> (HttpStatus.BAD_REQUEST);
        }else return new ResponseEntity<Collection<LinhaDeOnibus>> (result, HttpStatus.OK);
    }


    @GetMapping(value = "/onibus/{id}")
    public ResponseEntity<LinhaDeOnibus> getOnibusLinhaById(@PathVariable String id){

        Collection<LinhaDeOnibus> linhasDeOnibus =  getAllOnibusLinhas();

        LinhaDeOnibus result = new LinhaDeOnibus();


        for (LinhaDeOnibus onibus:linhasDeOnibus) {

            if(onibus.getId().equals(id)){
               result.setId(onibus.getId());
                result.setCodigo(onibus.getCodigo());
                result.setNome(onibus.getNome());
            }
        }


        if(result.getId()==null){

            return new ResponseEntity<LinhaDeOnibus> (HttpStatus.BAD_REQUEST);
        }else return new ResponseEntity<LinhaDeOnibus> (result, HttpStatus.OK);
    }

    @GetMapping(value = "/onibus/raio")
    public ResponseEntity<Collection<LinhaDeOnibus>> getOnibusLinhaByDistance(@RequestParam(name = "lat") String sLat, @RequestParam(name = "lng") String sLng, @RequestParam(name = "dist") String sDist){

        Collection<LinhaDeOnibus> linhasDeOnibus = getAllOnibusLinhas();
        Collection<LinhaDeOnibus> linhasNoRaio = new ArrayList<>();
        Double lat = Double.parseDouble(sLat);
        Double lng = Double.parseDouble(sLng);
        Double dist = Double.parseDouble(sDist);



        for (LinhaDeOnibus linha: linhasDeOnibus) {




            Itinerario itinerario =   this.itinerarioRestController.getItinerarioById(linha.getId());
            if(intersectaRota(itinerario, lat,lng, dist)){
                linhasNoRaio.add(linha);
            }

             try {
                 Thread.sleep(60);
             } catch (InterruptedException e) {
                 e.printStackTrace();
             }


         }




        return new ResponseEntity<Collection<LinhaDeOnibus>>(linhasNoRaio, HttpStatus.OK);
    }

    @GetMapping(value = "/lotacao/")
    public Collection<LinhaDeOnibus> getAllLotacaoLinhas(){


        Collection <LinhaDeOnibus> linhasDeOnibus = new ArrayList<>();
        String sLinhasDeOnibus =  linhaDeOnibusService.findAllLotacao();
        String dividido[] = sLinhasDeOnibus.split("},");
        for (String pedaco: dividido) {
            LinhaDeOnibus linhaDeOnibus = new LinhaDeOnibus();
            linhasDeOnibus.add(linhaDeOnibusContentParser.linhaDeOnibusParser(linhaDeOnibus, pedaco));
        }

        return linhasDeOnibus;
    }

    @GetMapping(value = "/lotacao/{name}")
    public ResponseEntity<Collection<LinhaDeOnibus>> getLinhasLotacaoByName(@PathVariable String name){

        Collection<LinhaDeOnibus> linhasDeOnibus =  getAllLotacaoLinhas();

        Collection<LinhaDeOnibus> result = new ArrayList<LinhaDeOnibus>();


        for (LinhaDeOnibus onibus:linhasDeOnibus) {

            if(onibus.getNome().contains(name) == true){
                result.add(onibus);

            }
        }


        if(result.isEmpty()){
            return new ResponseEntity<Collection<LinhaDeOnibus>> (HttpStatus.BAD_REQUEST);
        }else return new ResponseEntity<Collection<LinhaDeOnibus>> (result, HttpStatus.OK);
    }


    @GetMapping(value = "/lotacao/{id}")
    public ResponseEntity<LinhaDeOnibus> getLinhaById(@PathVariable String id){

        Collection<LinhaDeOnibus> linhasDeOnibus =  getAllLotacaoLinhas();

        LinhaDeOnibus result = new LinhaDeOnibus();


        for (LinhaDeOnibus onibus:linhasDeOnibus) {

            if(onibus.getId().equals(id)){
                result.setId(onibus.getId());
                result.setCodigo(onibus.getCodigo());
                result.setNome(onibus.getNome());
            }
        }


        if(result.getId()==null){

            return new ResponseEntity<LinhaDeOnibus> (HttpStatus.BAD_REQUEST);
        }else return new ResponseEntity<LinhaDeOnibus> (result, HttpStatus.OK);
    }

    @GetMapping(value = "/lotacao/raio")
    public ResponseEntity<Collection<LinhaDeOnibus>> getLinhaByDistance(@RequestParam(name = "lat") String sLat, @RequestParam(name = "lng") String sLng, @RequestParam(name = "dist") String sDist){

        Collection<LinhaDeOnibus> linhasDeOnibus = getAllLotacaoLinhas();
        Collection<LinhaDeOnibus> linhasNoRaio = new ArrayList<>();
        Double lat = Double.parseDouble(sLat);
        Double lng = Double.parseDouble(sLng);
        Double dist = Double.parseDouble(sDist);



        for (LinhaDeOnibus linha: linhasDeOnibus) {




            Itinerario itinerario =   this.itinerarioRestController.getItinerarioById(linha.getId());
            if(intersectaRota(itinerario, lat,lng, dist)){
                linhasNoRaio.add(linha);
            }

            try {
                Thread.sleep(60);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }



        return new ResponseEntity<Collection<LinhaDeOnibus>>(linhasNoRaio, HttpStatus.OK);
    }

    private boolean intersectaRota(Itinerario itinerario,Double lat, Double lng, Double meters ) {

        for (Geolocal coord: itinerario.getItinerario()) {

            double dist = GeoLocalization.haversine(Double.parseDouble(coord.getLat()), Double.parseDouble(coord.getLng()), lat, lng);
            if(dist <= meters){
                return true;
            }


        }



        return false;
    }

}
