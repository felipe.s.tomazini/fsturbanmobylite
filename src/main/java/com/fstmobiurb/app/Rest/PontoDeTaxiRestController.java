package com.fstmobiurb.app.Rest;

import com.fstmobiurb.app.model.PontoDeTaxi;
import com.fstmobiurb.app.service.PontoDeTaxiService;
import com.fstmobiurb.app.service.PontoDeTaxiServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Collection;

@RestController
@RequestMapping(value = "api/taxis")
public class PontoDeTaxiRestController {

   @Autowired
    private PontoDeTaxiServiceImpl pontoDeTaxiService;


    @GetMapping(value = "")
    public ResponseEntity<Collection<PontoDeTaxi>> getAllPontosDeTaxi(){
        Collection<PontoDeTaxi> pontosDeTaxi = pontoDeTaxiService.getAllPontosDeTaxi();
        ResponseEntity<Collection<PontoDeTaxi>> response = new ResponseEntity<>(pontosDeTaxi, HttpStatus.OK);
        return response;
    }

    @GetMapping(value = "/{nome}")
    public ResponseEntity<PontoDeTaxi> getAllPontosDeTaxi(@PathVariable String nome){
        PontoDeTaxi pontoDeTaxi = pontoDeTaxiService.getPontoDeTaxiByName(nome);
        ResponseEntity<PontoDeTaxi> response = new ResponseEntity<>(pontoDeTaxi, HttpStatus.OK);
        return response;
    }

    @PutMapping(value = "")
    public ResponseEntity<PontoDeTaxi> savePontoDeTaxi(@RequestBody PontoDeTaxi pontoDeTaxi){

        try {
            pontoDeTaxiService.savePontoDeTaxi(pontoDeTaxi);
        }catch (IOException e){
             ResponseEntity<PontoDeTaxi> response = new ResponseEntity(new PontoDeTaxi(),HttpStatus.BAD_REQUEST);
             return response;
        }

        ResponseEntity<PontoDeTaxi> response = new ResponseEntity(pontoDeTaxi,HttpStatus.OK);
        return response;

    }

    @DeleteMapping(value = "/{nome}")
    public ResponseEntity<Object> savePontoDeTaxi(@PathVariable String nome){

        try {
            pontoDeTaxiService.deletePontoDeTaxiByName(nome);
        }catch (IOException e){
            ResponseEntity<Object> response = new ResponseEntity(HttpStatus.BAD_REQUEST);
            return response;
        }

        ResponseEntity<Object> response = new ResponseEntity(HttpStatus.OK);
        return response;

    }

}
