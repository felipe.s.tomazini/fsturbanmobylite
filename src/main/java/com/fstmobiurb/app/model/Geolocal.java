package com.fstmobiurb.app.model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Geolocal {

    @Getter
    @Setter
    private Integer id;
    @Getter
    @Setter
    private String lat;
    @Getter
    @Setter
    private String lng;





}
