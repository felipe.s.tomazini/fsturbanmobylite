package com.fstmobiurb.app.model;


import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class Itinerario {

    @Getter
    @Setter
    private Integer idlinha;
    @Getter
    @Setter
    private String  nome;
    @Getter
    @Setter
    private String codigo;

    @Getter
    private List<Geolocal> itinerario = new ArrayList<Geolocal>();


    public Itinerario(Integer idlinha, String nome, String codigo) {
        this.idlinha = idlinha;
        this.nome = nome;
        this.codigo = codigo;
    }
}
