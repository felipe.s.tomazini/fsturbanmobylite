package com.fstmobiurb.app.model;


import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LinhaDeOnibus {
    @Getter
    @Setter
    private Integer id;
    @Getter
    @Setter
    private String codigo;
    @Getter
    @Setter
    private String nome;


}
