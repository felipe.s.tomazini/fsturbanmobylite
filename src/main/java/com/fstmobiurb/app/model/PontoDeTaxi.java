package com.fstmobiurb.app.model;

import lombok.*;

import java.util.Date;


@Data
@NoArgsConstructor
public class PontoDeTaxi {
    @Getter
    @Setter
    private String nomePonto;
    @Getter
    @Setter
    private Double latitude;
    @Getter
    @Setter
    private Double longitude;
    @Getter
    private Date momentoCadastro;

    public PontoDeTaxi(String nomePonto, Double latitude, Double longitude) {
        this.nomePonto = nomePonto;
        this.latitude = latitude;
        this.longitude = longitude;
        this.momentoCadastro = new Date();
    }

    public String pontoDeTaxiEncoder(){

        return (nomePonto + "#" + latitude + "#" + longitude +"#"+ momentoCadastro);
    }


}
