package com.fstmobiurb.app.model.util;

import com.fstmobiurb.app.model.Geolocal;
import com.fstmobiurb.app.model.Itinerario;

import java.util.ArrayList;
import java.util.List;

public class ItinerarioContentParser {



    public Itinerario itinerarioParser(Itinerario itinerario, String data){

        String pedacos[] = data.split("\",\"");



        pedacos[0] = pedacos[0].replace("{\"idlinha\":\"","" );
        pedacos[1] = pedacos[1].replace("nome\":\"","");
        pedacos[2] = pedacos[2].replace("codigo\":\"", "");

        itinerario.setIdlinha(Integer.parseInt(pedacos[0]));
        itinerario.setNome(pedacos[1]);
        itinerario.setCodigo(pedacos[2]);

        itinerario.setItinerario(getItinerarioList(data));

        return itinerario;
    }





    private List<Geolocal>getItinerarioList(String data)
    {
        List<Geolocal> itinerario = new ArrayList<Geolocal>();

        String informacao [] = data.split("lat");



        for(int i =1; i < informacao.length; i++){
            Geolocal geolocal = new Geolocal();

            String pedaco[] = informacao[i].split(",");
            pedaco[0]= pedaco[0].replace("\":\"","");
            pedaco[0]= pedaco[0].replace("\"","");
            pedaco[1]= pedaco[1].replace("\"lng\":\"", "");
            pedaco[1]= pedaco[1].replace("\"}", "");
            pedaco[1]= pedaco[1].replace("}", "");

            geolocal.setId(i-1);
            geolocal.setLat(pedaco[0]);
            geolocal.setLng(pedaco[1]);

            itinerario.add(geolocal);

        }



        return  itinerario;
    }
}
