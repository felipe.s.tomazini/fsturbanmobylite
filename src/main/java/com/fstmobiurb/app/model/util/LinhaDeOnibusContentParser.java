package com.fstmobiurb.app.model.util;

import com.fstmobiurb.app.model.LinhaDeOnibus;

public class LinhaDeOnibusContentParser {


    public LinhaDeOnibus linhaDeOnibusParser(LinhaDeOnibus linhaDeOnibus, String frase){

        String campos[] = frase.split(",");
        campos[0]=campos[0].replace("[{\"id\":", "");
        campos[0]=campos[0].replace("\"id\":","");
        campos[0]=campos[0].replace("{","");
        campos[0]=campos[0].replace("\"", "");
        campos[1]=campos[1].replace("\"codigo\":","");
        campos[1]= campos[1].replace("\"", "");
        campos[2]=campos[2].replace("\"nome\":","");
        campos[2]=campos[2].replace("\"", "");
        campos[2]=campos[2].replace("}]", "");

        linhaDeOnibus.setId(Integer.parseInt(campos[0]));
        linhaDeOnibus.setCodigo(campos[1]);
        linhaDeOnibus.setNome(campos[2]);



        return linhaDeOnibus;
    }
}
