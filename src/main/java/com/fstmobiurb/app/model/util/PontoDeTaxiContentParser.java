package com.fstmobiurb.app.model.util;

import com.fstmobiurb.app.model.PontoDeTaxi;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class PontoDeTaxiContentParser {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");

    public PontoDeTaxi pontoDeTaxiParser(String data){
        PontoDeTaxi pontoDeTaxi = new PontoDeTaxi();
        String campos[] = data.split("#");
        try{
        pontoDeTaxi.setNomePonto(campos[0]);
        pontoDeTaxi.setLatitude(Double.parseDouble(campos[1]));
        pontoDeTaxi.setLongitude(Double.parseDouble(campos[2]));
        pontoDeTaxi.setMomentoCadastro(sdf.parse(campos[3]));
        }catch (IllegalArgumentException e){
            System.out.println("Error in parse: " + e.getMessage() );
        }catch (ParseException e){
            System.out.println("Error in parse: " + e.getMessage() );
        }
        return pontoDeTaxi;

    }


}
