package com.fstmobiurb.app.repository;

import com.fstmobiurb.app.service.ItinerarioService;

public interface ItinerarioRepository extends ItinerarioService {

    String findItinerarioById();
}
