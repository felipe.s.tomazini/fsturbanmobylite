package com.fstmobiurb.app.repository;

import com.fstmobiurb.app.model.LinhaDeOnibus;
import com.fstmobiurb.app.service.LinhaDeOnibusService;

import java.util.Collection;

public interface LinhaDeOnibusRepository extends LinhaDeOnibusService {

    String findAllOnibus();

}
