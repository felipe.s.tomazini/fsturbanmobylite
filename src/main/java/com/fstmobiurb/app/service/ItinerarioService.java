package com.fstmobiurb.app.service;

import com.fstmobiurb.app.model.Itinerario;
import com.fstmobiurb.app.repository.ItinerarioRepository;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;


@Service
@FeignClient(name = "poaItinerario", url = "http://www.poatransporte.com.br/" )
public interface ItinerarioService {

        @GetMapping(value = "/php/facades/process.php?a=il&p={id}")
        String findItinerarioById(@PathVariable Integer id);

 //       String findItinerarioByName(@PathVariable String name);

 //       void saveItinerario(@RequestBody Itinerario itinerario);

  //      void deleteItinerarioById(@PathVariable Integer id);
}
