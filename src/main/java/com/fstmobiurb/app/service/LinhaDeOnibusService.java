package com.fstmobiurb.app.service;

import com.fstmobiurb.app.model.LinhaDeOnibus;
import com.fstmobiurb.app.repository.LinhaDeOnibusRepository;
import feign.jackson.JacksonDecoder;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Collection;

@Service
@FeignClient(name = "poaLinhas", url = "http://www.poatransporte.com.br/")
public interface LinhaDeOnibusService{

    @GetMapping(value = "php/facades/process.php?a=nc&p=%&t=o")
    String findAllOnibus();
    @GetMapping
    String findOnibusById(@PathVariable Integer id);

    @GetMapping
    String findOnibusByName(@PathVariable String name);

    @GetMapping(value = "php/facades/process.php?a=nc&p=%&t=l")
    String findAllLotacao();
    @GetMapping
    String findLotacaoById(@PathVariable Integer id);

    @GetMapping
    String findlotacaoByName(@PathVariable String name);

   // void saveOnibus(@RequestBody LinhaDeOnibus linhaDeOnibus);

   // void deleytOnibusById(@PathVariable Integer id);




}
