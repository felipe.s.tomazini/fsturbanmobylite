package com.fstmobiurb.app.service;

import com.fstmobiurb.app.model.PontoDeTaxi;

import java.io.IOException;
import java.util.Collection;

public interface PontoDeTaxiService {

    Collection<PontoDeTaxi> getAllPontosDeTaxi();

    PontoDeTaxi getPontoDeTaxiByName(String name);

    void savePontoDeTaxi(PontoDeTaxi pontoDeTaxi) throws IOException;

    void deletePontoDeTaxiByName(String name) throws IOException;

}
