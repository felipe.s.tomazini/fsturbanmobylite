package com.fstmobiurb.app.service;

import com.fstmobiurb.app.model.PontoDeTaxi;
import com.fstmobiurb.app.model.util.PontoDeTaxiContentParser;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class PontoDeTaxiServiceImpl implements PontoDeTaxiService {

    private String path ="src\\main\\resources\\taxiData\\taxiData.txt";
    private PontoDeTaxiContentParser parser = new PontoDeTaxiContentParser();

    @Override
    public Collection<PontoDeTaxi> getAllPontosDeTaxi() {
        Collection<PontoDeTaxi> pontosDeTaxi = new ArrayList<>();

        try(BufferedReader br = new BufferedReader(new FileReader(path))) {

            String line ="";
            do{

                line = br.readLine();
                if(line != null)
                {
                   PontoDeTaxi pontoDeTaxi = parser.pontoDeTaxiParser(line);
                   pontosDeTaxi.add(pontoDeTaxi);
                }
            }while (line != null);
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }

        return pontosDeTaxi;
    }

    @Override
    public PontoDeTaxi getPontoDeTaxiByName(String name) {
        PontoDeTaxi pontoDeTaxi = new PontoDeTaxi();

        try(BufferedReader br = new BufferedReader(new FileReader(path))) {

            String line ="";
            do{

                line = br.readLine();
                if(line != null)
                {
                    pontoDeTaxi = parser.pontoDeTaxiParser(line);
                    if(pontoDeTaxi.getNomePonto().equals(name)){
                        return pontoDeTaxi;
                    }
                }
            }while (line != null);
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }

        return null;
    }

    @Override
    public void savePontoDeTaxi(PontoDeTaxi pontoDeTaxi) throws IOException {
        List<String> linhas = new ArrayList<String>();
        boolean saved = false;

        try(BufferedReader br = new BufferedReader(new FileReader(path))) {

            String line ="";
            do {

                line = br.readLine();
                if (line != null) {
                    linhas.add(line);


                }

            }while (line != null) ;

        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }

        for (String linha: linhas) {
            System.out.println(linha);
        }

        try(BufferedWriter bw = new BufferedWriter(new FileWriter(path))){

            for(int i = 0; i < linhas.size(); i++ ) {
                PontoDeTaxi aux = parser.pontoDeTaxiParser(linhas.get(i));

                if (aux.getNomePonto().equals(pontoDeTaxi.getNomePonto())) {
                    bw.write(pontoDeTaxi.pontoDeTaxiEncoder());
                    bw.newLine();
                    saved = true;

                } else {
                    bw.write(linhas.get(i));
                    bw.newLine();
                }

            }
            if(saved == false){
                bw.write(pontoDeTaxi.pontoDeTaxiEncoder());
            }

        }catch (IOException e){
            e.getMessage();
        }

    }

    @Override
    public void deletePontoDeTaxiByName(String name) throws IOException {
        List<String> linhas = new ArrayList<String>();
        boolean saved = false;

        try(BufferedReader br = new BufferedReader(new FileReader(path))) {

            String line ="";
            do {

                line = br.readLine();


                if (line != null) {
                    PontoDeTaxi pontoDeTaxi = parser.pontoDeTaxiParser(line);
                    if(pontoDeTaxi.getNomePonto().equals(name)){
                        System.out.println();
                    }else linhas.add(line);

                }

            }while (line != null) ;

        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }

        for (String linha: linhas) {
            System.out.println(linha);
        }

        try(BufferedWriter bw = new BufferedWriter(new FileWriter(path))){

            for (String linha: linhas) {
                bw.write(linha);
                bw.newLine();
            }
        }catch (IOException e){
            e.getMessage();
        }

    }
}

